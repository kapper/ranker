require "kemal"
require "http/client"
require "json"

module Ranker
  VERSION = "0.1.0"

  before_all do |env|
    env.response.headers["Access-Control-Allow-Origin"] = "*"
  end

  get "/" do
    "Hello, world!"
  end

  get "/:steam_id" do |env|
    steam_id = env.params.url["steam_id"]
    next "" unless steam_id =~ /^\d+$/
    response = HTTP::Client.get("http://101.200.189.65:431/dac/heros/get/@#{steam_id}")
    info = JSON.parse(response.body)["user_info"][steam_id].as_h
    steam_response = HTTP::Client.get(
      "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2/?key=3C14130D08CBB3A040B4ABE92F8D7655&format=json&steamids=#{steam_id}"
    )
    steam_info = JSON.parse(steam_response.body)["response"]["players"][0].as_h
    {
      name: steam_info["personaname"].as_s,
      candy: info["candy"].as_i,
      matches: info["match"].as_i,
      rank: info["mmr_level"].as_i,
      human_rank: Rank.from_i(info["mmr_level"].as_i).to_s,
      avartar_url: steam_info["avatarfull"].as_s
    }.to_json
  end

  class Rank
    def self.from_i(i : Int32)
      new(i)
    end

    def initialize(@i : Int32)
    end

    def to_s
      "#{piece(@i)} #{level(@i)}"
    end

    def piece(i)
      case i
      when (1..9)
        "Pawn"
      when (10..18)
        "Knight"
      end
    end

    def level(i)
      (i % 9)
    end
  end
end

Kemal.run
